﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c7202319_Component2
{
    /// <summary>
    /// Class Circle for creating rectangle shape
    /// </summary>
    /// <seealso cref="c7202319_Component2.Shape" />
    class Circle : Shape
    {
        private int r, x, y;
        
        public void saved_values(int a, int b, int c) {
            x = a;
            y = b;
            r = c;
        }
        public void Draw_shape(Graphics g)
        {
         
            Pen mew2 = Form1.defaultpen;
            SolidBrush me = Form1.sbrush;
            if (Form1.fill)
            {
                g.FillEllipse(me, x, y, r, r);     //gives filled ellipse 
            }
            else
            {
                g.DrawEllipse(mew2, x, y, r, r);     //gives not filled ellipse
            }







        }

     
    }
}
