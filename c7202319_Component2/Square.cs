﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c7202319_Component2
{
    /// <summary>
    /// Class for square shape creating
    /// </summary>
    class Square:Shape
    {

        private int length, x, y;
        /// <summary>
        /// Saveds the values.
        /// </summary>Method for saving value of square
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        public void saved_values(int a, int b, int c)    
        {
            x = a;
            y = b;
            length = c;
        }
        public void Draw_shape(Graphics g)
        {
            
            Pen mew2 = Form1.defaultpen;
            SolidBrush me = Form1.sbrush;
            if (Form1.fill)
            {
                g.FillRectangle(me, x, y, length, length);     //gives filled ellipse 
            }
            else
            {
                g.DrawRectangle(mew2, x, y, length, length);     //gives not filled ellipse
            }

        }


    }
}
