﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c7202319_Component2
{
    /// <summary>
    /// Class Rectangle for creating rectangle shape
    /// </summary>
    /// <seealso cref="c7202319_Component2.Shape" />
    class Rectangle :Shape
    {
        private int x, y, w, h;              //positions and width, height passed.
       
        public void saved_values(int a, int b, int c, int d)
        {
            x = a;
            y = b;
            w = c;
            h = d;
        }
        
        public void Draw_shape(Graphics g)
        {
           

            Pen pen = Form1.defaultpen;    //pen set to deafault. i.e. black   
            SolidBrush brush = Form1.sbrush;   //brush set to deafault. i.e. black   
            if (Form1.fill)
            {
                g.FillRectangle(brush, x, y, w, h);    //filled rectangle drawn
            }
            else
            {
                g.DrawRectangle(pen, x, y, w, h);      //not filled rectangle drawn
            }



        }


        


        }
}
