﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c7202319_Component2
{
    /// <summary>
    /// Class for triangle shape creating
    /// </summary>
    class Triangle : Shape
    {
        /// <summary>
        /// values needed for drawing circle
        /// </summary>
        private int x, y, bas, per;        

        /// <summary>
        /// saved values for circle parameters and position
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void saved_values(int a, int b, int c, int d)
        {
            x = a;
            y = b;
            bas = c;
            per = d;


        }
        public void Draw_shape(Graphics g)       //method for drawing shape of triangle
        {
            
            PointF A = new PointF(x, y);
            PointF B = new PointF(x + per, y);
            PointF C = new PointF(B.X, B.Y + per);
            PointF[] pnt = { A, B, C };
            

            Pen pen = Form1.defaultpen;
            SolidBrush brush = Form1.sbrush;
            if (Form1.fill)
            {
                g.FillPolygon(brush, pnt);          //filled triangle is produced.
            }
            else
            {
                g.DrawPolygon(pen, pnt);
            }
        }


        


    }
}

