﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;
namespace c7202319_Component2
{
   /// <summary>
   /// Class for getting graphic contents.
   /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// position of X-axis initialized to zero.
        /// </summary>
        public int position_Xaxis = 0;
        /// <summary>
        /// position of Y-axis initialized to zero.
        /// </summary>
        public int position_Yaxis = 0;
        /// <summary>
        /// Commands given to string array
        /// </summary>
        string[] commands = {"drawto", "moveto", "rectangle", "square", "circle","triangle", "fill", "pen"};
        /// <summary>
        /// Boolean declared for drawing
        /// </summary>
        public bool drawing = false;
        /// <summary>
        /// Boolean declared for loading file
        /// </summary>
        public bool loading = false;
        /// <summary>
        /// Boolean declared for method
        /// </summary>
        bool method = false;
        /// <summary>
        /// Boolean declared for saving file
        /// </summary>
        public bool save = false;
        /// <summary>
        /// boolean declared for executing
        /// </summary>
        public bool execute = false;
        /// <summary>
        /// boolean declared for clearing field
        /// </summary>
        public bool clearing = false;
        List<String[]> n = new List<String[]>();
        List<String> body = new List<string>();
        List<String> b = new List<String>();
        List<int> p = new List<int>();
        List<String> pii = new List<String>();
        /// <summary>
        /// boolean declared for resetting positions
        /// </summary>
        public bool reset_bool = false;
        /// <summary>
        /// int declared for counting lines
        /// </summary>
        public int lineCount = 1;
        /// <summary>
        /// int declared for getting line number
        /// </summary>
        public int lineNumberCount = 0;
        /// <summary>
        /// int cleared for counting if
        /// </summary>
        public int IfCounter = 0;
        /// <summary>
        /// Dictionary declared for storing variables in string
        /// </summary>
        public Dictionary<string, string> variableDictionary = new Dictionary<string, string>();
        /// <summary>
        /// Array of string for storing syntax
        /// </summary>
        String[] syntax;
        /// <summary>
        /// Boolean declared for filling shapes
        /// </summary>
        public static bool fill = false;
        /// <summary>
        /// Default color of pen is assigned to black color.
        /// </summary>
        public static Pen defaultpen = new Pen(Color.Black);
        /// <summary>
        /// Default color of brush is assigned to black color.
        /// </summary>
        public static SolidBrush sbrush = new SolidBrush(Color.Black);

        

       /// <summary>
       /// Method called penswitcher for switching colors of pen as user enters
       /// </summary>
       /// <param name="pencolor"></param>
        



        public void penswitcher(string pencolor)
        {
            if (!fill)                        //if fill is not to be applied
            {
                switch (pencolor)                     
                {
                    case "blue":           //if color is blue
                        {
                            defaultpen = new Pen(Color.Blue);      //defaultpen set to blue
                            break;
                        }

                    case "green":
                        {
                            defaultpen = new Pen(Color.Green);
                            break;
                        }

                    case "yellow":
                        {
                            defaultpen = new Pen(Color.Yellow);
                            break;
                        }
                    case "red":
                        {
                            defaultpen = new Pen(Color.Red);
                            break;
                        }

                }
                MessageBox.Show("Pen color changed to " + pencolor);   //message after changing color

            }
        }

        /// <summary>
        /// Method called penswitcher1 for switching colors of fill as user enters
        /// </summary>
        /// <param name="fillcolor"></param>
        public void penswitcher1(string fillcolor)
        {
            
                switch (fillcolor)                     
                {
                    case "blue":                          //if fill color is blue
                    {
                            sbrush = new SolidBrush(Color.Blue);
                            break;
                        }

                    case "green":
                        {
                            sbrush = new SolidBrush(Color.Green);
                            break;
                        }

                    case "yellow":
                        {
                            sbrush = new SolidBrush(Color.Yellow);
                            break;
                        }

                    case "red":
                        {
                            sbrush = new SolidBrush(Color.Red);
                            break;
                        }

                

                
            }
            MessageBox.Show("Fill color changed to " + fillcolor);  //message after changing color
        }
        


        /// <summary>
        /// Constructor of the class
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }
       
        /// <summary>
        /// Method for moving position of pen
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void pen_to_move(int x, int y)
        {
            Pen mew2 = Form1.defaultpen;
            Pen pen_move = new Pen(Color.Black, 2);
            position_Xaxis = x;
            position_Yaxis = y;
        }


        /// <summary>
        /// Method for taking commands and giving result according to command.
        /// All the commands work from here.
        /// </summary>
        public void suj()
        {
            string command = programbox.Text;
            String[] multiLine = command.Split(')');

            for (int i = 0; i < multiLine.Length - 1; i++)
            {
                string abc = multiLine[i].Trim();
                syntax = abc.Split('(');

                if (syntax[0].Equals("pen") == true)
                {
                    this.penswitcher(syntax[1]);


                }

                else if (syntax[0].Equals("fill") == true)
                {

                    this.penswitcher1(syntax[1]);
                    fill = true;

                    if (syntax[1] == "off")
                    {
                        fill = false;
                    }

                }

            }
        
        


             if (string.Compare(syntax[0].ToLower(), "moveto") == 0)
            {
                String[] parameter1 = syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("MoveTo Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String[] parameter2 = parameter1[1].Split(')');
                    String one = parameter1[0];
                    String two = parameter2[0];
                    pen_to_move(int.Parse(one), int.Parse(two));
                    if (parameter1.Length > 1 && parameter1.Length < 3)
                        pen_to_move(int.Parse(one), int.Parse(two));
                    else
                        throw new ArgumentException("MoveTo takes Only 2 Parameters");

                }
            }

          






            //executes if "drawto" command is triggered
            else if (string.Compare(syntax[0].ToLower(), "drawto") == 0)
            {

                String[] parameter1 = syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("DrawTo Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String[] parameter2 = parameter1[1].Split(')');
                    String one = parameter1[0];
                    String two = parameter2[0];
                    pentodraw(int.Parse(one), int.Parse(two));
                    if (parameter1.Length == 2)
                        pentodraw(int.Parse(one), int.Parse(two));

                    else
                    {
                        throw new ArgumentException("DrawTo Takes Only 2 Parameters");
                    }
                }

            }
            //clear command clears the drawing area.
            else if (string.Compare(syntax[0].ToLower(), "clear") == 0)
            {
                clear();
            }
            //reset command resets to initial position.
            else if (string.Compare(syntax[0].ToLower(), "reset") == 0)
            {
                reset();
            }
            //executes when rectangle command is entered
            else if (string.Compare(syntax[0].ToLower(), "rectangle") == 0)
            {
                String[] parameter1 = syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("Rectangle Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String[] parameter2 = parameter1[1].Split(')');
                    String one = parameter1[0];
                    String two = parameter2[0];
                    if (parameter1.Length > 1 && parameter1.Length < 3)
                        rectangle_draw(position_Xaxis, position_Yaxis, int.Parse(one), int.Parse(two));
                    else
                        throw new ArgumentException("Rectangle Takes Only 2 Parameters");
                }
            }

            //executes when square command is entered
            else if (string.Compare(syntax[0].ToLower(), "square") == 0)
            {
                String test = syntax[1];
                String[] parameter2 = syntax[1].Split(')');
                if (!test.Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String two = parameter2[0];
                    if (two != null || two != "" || two != " ")
                        square_draw(position_Xaxis, position_Yaxis, int.Parse(two));
                    else
                        throw new ArgumentException("Square Takes Only 1 Parameter");

                }
            }
            //executes when circle command is entered
            else if (string.Compare(syntax[0].ToLower(), "circle") == 0)
            {
                String test = syntax[1];
                String[] parameter2 = syntax[1].Split(')');
                if (!test.Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String two = parameter2[0];
                    if (two != null || two != "" || two != " ")
                        circle_draw(position_Xaxis, position_Yaxis, int.Parse(two));
                    else
                        throw new ArgumentException("Circle Takes Only 1 Parameter");

                }
            }
            //executes when triangle command is entered
            else if (string.Compare(syntax[0].ToLower(), "triangle") == 0)
            {
                String[] parameter1 = syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("Triangle Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String[] parameter2 = parameter1[1].Split(')');
                    String one = parameter1[0];
                    String two = parameter2[0];
                    if (parameter1.Length > 1 && parameter1.Length < 3)
                        triangle_draw(position_Xaxis, position_Yaxis, int.Parse(one), int.Parse(two));
                    else
                        throw new ArgumentException("Triangle Takes Only 2 Parameters");
                }
            }




        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Method for drawing line
        /// </summary>
        /// <param name="a">First position given by user</param>
        /// <param name="b">Second position given by user</param>
        public void pentodraw(int a, int b)
        {
            Pen mew = new Pen(Color.Black, 2);
            Graphics g = panel1.CreateGraphics();
            g.DrawLine(mew, position_Xaxis, position_Yaxis, a, b);
            position_Xaxis = a;
            position_Yaxis = b;
        }
        
        /// <summary>
        /// Method for checking the command and generating error if wrong command
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkCommand(string line)
        {
            for (int a = 0; a < commands.Length; a++)
            {
                if (line.Contains(commands[a]))
                {
                    return true;
                }
            }
            String[] temp = line.Split('(');
            if (b.Contains(temp[0])) {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Button for running the program.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button2_Click(object sender, EventArgs e)
        {
            variableDictionary.Clear();
            method = false;
            lineCount = 1;
            commandbox.Clear();
            execute = false;
            var multi_command = programbox.Text;
            string[] multi_syntax = multi_command.Split(new char[] { '\n'}, StringSplitOptions.RemoveEmptyEntries);

            string command = programbox.Text;
            String[] multiLine = command.Split(')');

            for (int i = 0; i < multiLine.Length - 1; i++)
            {
                string abc = multiLine[i].Trim();
                syntax = abc.Split('(');

                if (syntax[0].Equals("pen") == true)
                {
                    this.penswitcher(syntax[1]);

                }

                else if (syntax[0].Equals("fill") == true)
                {
                    this.penswitcher1(syntax[1]);
                    fill = true;
                    if (syntax[1] == "off")
                    {
                        fill = false;
                    }

                }

            }




            foreach (string l in multi_syntax) 
            {
                bool result = caseRun(l);
                if (!result)
                {
                    panel1.Invalidate();
                    break;
                }
lineCount++;
 }
         }

        /// <summary>
        /// Method for variables, loop and method
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
            public bool caseRun(string line)
            {
                line = line.ToLower().Trim();

            if (IfCounter != 0)
            {
                IfCounter--;
                return true;
            }

            if (lineNumberCount != 0)
            {
                
                lineNumberCount--;

                return true;
            }

            else if (checkCommand(line))
            {

                string[] m_syntax = line.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
                if (!runShape(m_syntax, line))
                    return false;


            }
            else if(checkVariableDec(line)){
                try
                {
                    string[] variableDeclration = line.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);
                    string key = variableDeclration[0];
                    string value = variableDeclration[1];
                    variableDictionary.Add(key,value);

                }
                catch (Exception e)
                {
                    commandbox.Text = "Line: " + lineCount + " Invalid Declaration of variable";
                    return false;
                }

            }else if (checkIfElse(line))
            {
                bool endIfCheck = false;
                bool conditionStatus = false;
                string conditionOperator = "";
                try
                {

                    string[] IfCondtionParameter = getIfParameter(line);

                    foreach(string c in IfCondtionParameter){
                        Console.WriteLine(c);
                    }


                    if (!variableDictionary.ContainsKey(IfCondtionParameter[0].Trim().ToLower()))
                    {
                        commandbox.Text = "Line: " + lineCount + " Variable doesn't exist";
                        return false;
                    }


                    string condValueString = IfCondtionParameter[1];
                    int condValue = Int32.Parse(condValueString);




                   string v = IfCondtionParameter[0].Trim().ToLower();
                    string varValuestring = variableDictionary[v];
                    int varvalue = Int32.Parse(varValuestring);

                    if (line.Contains("="))
                    {
                        conditionOperator = "=";

                    }else if (line.Contains("<"))
                    {
                        conditionOperator = "<";
                    }
                    else if (line.Contains(">"))
                    {
                        conditionOperator = ">";
                    }


                    var multi_command = programbox.Text;
                    string[] multi_syntax = multi_command.Split(new char[] { '\n' },StringSplitOptions.RemoveEmptyEntries);

                    foreach (string l in multi_syntax)
                    {

                        if(l.ToLower().Trim() == "endif")
                        {
                            endIfCheck = true;
                            break;
                        }
                       
                    }
                    if (!endIfCheck)
                    {
                        commandbox.Text = "Line: " + lineCount + " If EndIf statement not closed.";
                        return false;
                    }


                    if(conditionOperator == "=")
                    {
                        if(varvalue == condValue)
                        {
                            conditionStatus = true;
                        }
                    }else if( conditionOperator == "<"){


                        if(varvalue < condValue)
                        {
                            conditionStatus = true;
                        }
                    }else if (conditionOperator == ">")
                    {
                        if (varvalue > condValue)
                        {
                            conditionStatus = true;
                        }
                    }
                    for (int i = lineCount; i < multi_syntax.Length; i++)
                    {
                        if(multi_syntax[i] == "endif")
                        {
                            break;
                        }
                        else
                        {
                            IfCounter++;
                        }
                    }

                        if (conditionStatus)
                    {
                        IfCounter = 0;
                    }

                    }
                catch (Exception e)
                {
                    commandbox.Text = "Line: " + lineCount + " Invalid if else statement" +"\n" + e.Message;
                    return false;
                }

            }else if(line=="endif"){
                return true;
            }
            else if(checkVariableOperation(line)){
                //check variable operation
                string[] variable = line.Split(new char[] { '+','-' }, 2, StringSplitOptions.RemoveEmptyEntries);
                string variableOperator = "";

                if (line.Contains("+"))
                {
                    variableOperator = "+";

                }else
                {
                    variableOperator = "-";
                }
                string realKey = variable[0];
                int realValue=Int32.Parse(variable[1]);
                int dictValue = Int32.Parse(variableDictionary[realKey]);
                if (!variableDictionary.ContainsKey(realKey))
                {
                    commandbox.Text = "Line: " + lineCount + " Variable doesn't Exist";
                    return false;
                }


                if(variableOperator == "+")
                {
                    variableDictionary[realKey] = (dictValue + realValue).ToString();
                }
                else
                {
                    variableDictionary[realKey] = (dictValue - realValue).ToString();
                }
         
            }
            else if (checkLoop(line))
            {

                bool endloopCheck = false;
                try
                {
                    string[] loop = line.Split(new string[] { "for" }, 2, StringSplitOptions.RemoveEmptyEntries);

                    string loopCondition = loop[1].Trim();

                    string[] loopVariable = loopCondition.Split(new string[] { "<=",">=" }, 2, StringSplitOptions.RemoveEmptyEntries);

                    foreach(string l in loopVariable)
                    {
                        Console.WriteLine(l);
                    }

                    foreach(KeyValuePair<string,string> k in variableDictionary)
                    {
                        Console.WriteLine(k.Key +"=" +k.Value);
                    }
                    int loopValue = Int32.Parse(loopVariable[1].Trim()) ;
                    string loopKey = loopVariable[0].Trim();
                    Console.WriteLine(loopKey);
                    if (!variableDictionary.ContainsKey(loopKey))
                    {
                        commandbox.Text = "Line: " + lineCount + " Variable doesn't exist 2";
                        return false;
                    }


                    var multi_command = programbox.Text;
                    string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string l in multi_syntax)
                    {

                        if (l.ToLower().Trim() == "endloop")
                        {
                            endloopCheck = true;
                            break;
                        }

                    }
                    if (!endloopCheck)
                    {
                        commandbox.Text = "Line: " + lineCount + " loop not closed.";
                        return false;
                    }


                    //endloop check

                   
                    
                    int counterLine = 0;
                    int lineNumberCount1 = 0;

                    List<string> loopList = new List<string>();
                    for (int i = lineCount; i < multi_syntax.Length; i++)
                    {
                       
                        if (multi_syntax[i] == "endloop")
                        {

                            break;
                        }
                        else
                        {
                            lineNumberCount1++;
                            loopList.Add(multi_syntax[i]);
                        }
                    }



                    int dictValue = 0;
                    string loopOperator = "";
                    counterLine = lineCount;

                    if (line.Contains("<="))
                    {
                        loopOperator = "<=";
                    }
                    else
                    {
                        loopOperator = ">=";
                    }


                    if (loopOperator == "<=")
                    {
                        while (dictValue <= loopValue)
                        {
                            lineCount = counterLine;
                            foreach (string list in loopList)
                            {
                                lineCount++;
                                if (!caseRun(list))
                                    return false;
                            }
                            dictValue = Int32.Parse(variableDictionary[loopKey]);
                        }
                    }
                    else
                    {

                        while (dictValue >= loopValue)
                        {
                            lineCount = counterLine;
                            foreach (string list in loopList)
                            {
                                lineCount++;
                                if (!caseRun(list))
                                    return false;
                            }
                            dictValue = Int32.Parse(variableDictionary[loopKey]);
                        }
                    }

                    lineCount = counterLine;
                    lineNumberCount = lineNumberCount1;
                }
                catch (Exception e)
                {
                    //exception for invalid loop statement
                    commandbox.Text = "Line: " + lineCount + " Invaild Loop Statement + \n " + e.Message;
                    return false;
                }


            }
            else if(line=="endloop"){       //after executing endloop
                return true;
            }else if (checkMethod(line))
            {
                bool endMethodCheck = false;
                var multi_command = programbox.Text;
                string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = lineCount; i < multi_syntax.Length; i++)
                {

                    if (multi_syntax[i] == "endmethod")
                    {
                        endMethodCheck = true;
                        break;
                    }
                   
                }

                if (!endMethodCheck)
                {
                    commandbox.Text = "Line: " + lineCount + "Method not closed" ;
                    return false;
                }



                    String[] area = multi_command.Split('\n');
                    String blank1 = "";
                    String blank2 = "";

                
                    for (int i = 0; i < area.Length; i++)
                    {
                        if (area[i].ToLower().Contains("method"))
                        {
                            
                            String[] t = area[i].Split(' ');
                            String[] nn = t[t.Length - 1].Split('(');
                            if (nn[nn.Length - 1].Contains(','))
                            {
                                String[] g = nn[nn.Length - 1].Split(',');
                                p.Add(g.Length);
                                for (int x = 0; x < g.Length; x++)
                                {
                                    if (x == g.Length - 1)
                                    {
                                        String[] tt = g[x].Split(')');
                                        blank2 = blank2 + tt[0] + "\n";
                                    }
                                    else
                                    {
                                        blank2 = blank2 + g[x] + "\n";
                                    }
                                }
                                pii.Add(blank2);
                            }
                            else
                            {
                                String[] g = nn[nn.Length - 1].Split(')');
                                if (g[0] == " " || g[0] == null || g[0] == "")
                                {
                                    p.Add(0);
                                    pii.Add(" ");
                                }
                                else
                                {
                                    p.Add(1);
                                    blank2 = blank2 + g[0] + "\n";
                                    pii.Add(blank2);
                                }
                            }
                            b.Add(nn[0].ToLower());
                            
                        }
                        else
                        {
                            blank1 = blank1 + area[i] + "\n";
                        }
                    }
                   
                body.Add(blank1);
                    n.Add(blank1.Split('\n'));
                    method = true;


            }else if (line == "endmethod")
            {

            }
            else
            {
                commandbox.Text = "Line: " +lineCount+" Command doesn't Exist";
                return false;
            }

            
                return true;
            }

        /// <summary>
        /// Method for checking loop
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkLoop(string line)
        {
            if (line.StartsWith("loop"))
                return true;

            return false;

        }
        /// <summary>
        /// Method for getting if parameter
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public string[] getIfParameter(string line)
        {

            int start = line.IndexOf("(") + 1;
            int end = line.IndexOf(")", start);

            string result = line.Substring(start, end - start);
            string[] parameterList = result.Split(new Char[] { '>','<','=' },2,
                StringSplitOptions.RemoveEmptyEntries);

            return parameterList;


        }

        /// <summary>
        /// Method for checking variable operators
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkVariableOperation(string line)
        {
            if (line.Contains("+") || line.Contains("-"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method for getting parameter
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public string[] getParameter(string line)
        {

            int start = line.IndexOf("(") + 1;
            int end = line.IndexOf(")", start);

            string result = line.Substring(start, end - start);
            string[] parameterList = result.Split(new Char[] { ','}, 2, 
                StringSplitOptions.RemoveEmptyEntries);

            return parameterList;


        }

        /// <summary>
        /// Method for checking if else 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkIfElse(string line)
        {
            if (line.StartsWith("if"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method for checking if line starts with method
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkMethod(string line)
        {
            if (line.StartsWith("method"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method for checking variables like if, method, loop and = operator
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkVariableDec(string line)
        {
            if (line.Contains("=") && !line.StartsWith("if") && !line.StartsWith("method") && 
                !line.StartsWith("loop"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method for checking lines and variables
        /// </summary>
        /// <param name="lines">Lines within textbox</param>
        /// <returns></returns>
        public bool withVariable(string[] lines)
        {
            string line = "(" + lines[1];
            string[] parameters = getParameter(line);

            foreach (string param in parameters)
            {
                Console.WriteLine(param);
            }
                bool variableCheck = false;

            foreach(string param in parameters)
            {
                bool isNumeric = int.TryParse(param, out _);

                if (!isNumeric)
                {
                    variableCheck = true;
                    if (!variableDictionary.ContainsKey(param))
                    {
                        return false;
                    }
                    else
                    {
                       line= line.Replace(param, variableDictionary[param]);
                    }
                }
            }



            if (!variableCheck)
                return false;

            line = lines[0] + line;
            Console.WriteLine(line);
            caseRun(line);
          

            return true;
        }

        /// <summary>
        /// Method for running shapes
        /// </summary>
        /// <param name="m_syntax"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool runShape(string[] m_syntax, String line)
        {
           
            if (!method)
            {
                if (withVariable(m_syntax))
                {
                    return true;

                }
            }


            try
            {
                
                if (!method)
                {
                    if (b.Contains(m_syntax[0].ToLower()))
                    {
                        String mina = line;
                       
                        String mino = mina.Split('(')[0].Trim();
                        if (b.Contains(mino.Trim()))
                        {
                            int count = 0;
                            String[] l = mina.Split('(');
                            List<int> van = new List<int>();
                            if (l[l.Length - 1].Contains(','))
                            {
                                String[] alex = l[l.Length - 1].Split(',');
                                count = alex.Length;
                                for (int q = 0; q < alex.Length; q++)
                                {
                                    if (q != alex.Length - 1)
                                    {
                                        van.Add(int.Parse(alex[q]));
                                    }
                                    else
                                    {
                                        String[] fabin = alex[q].Split(')');
                                       
                                        if (fabin[0] != "" && fabin[0] != " " && fabin[0] == null)
                                            van.Add(int.Parse(fabin[0]));
                                        else
                                            van.Add(int.Parse(fabin[0]));
                                    }
                                }
                               
                                for (int k = 0; k < van.Count(); k++) {
                                   
                                }
                            }
                            else
                            {
                                String[] g = l[l.Length - 1].Split(')');
                                if (g[0] == " " || g[0] == null || g[0] == "")
                                {
                                    count = 0;
                                }
                                else
                                {
                                    van.Add(int.Parse(g[0]));
                                    count = 1;
                                }
                            }
                            if (count == p[b.IndexOf(mino.Trim())])
                            {
                                String[] rob = body[b.IndexOf(mino.Trim())].Split('\n');
                                String son = pii[b.IndexOf(mino.Trim())];

                                for (int salah = 0; salah < rob.Length; salah++)
                                {
                                    if (!string.IsNullOrEmpty(rob[salah]))
                                    {
                                        if (son == " ")
                                        {
                                           
                                            caseRun(rob[salah]); 
                                        }
                                        else
                                        {
                                            String[] io = son.Split('\n');
                                            for (int ui = 0; ui < io.Length; ui++)
                                            {
                                                if (!string.IsNullOrEmpty(io[ui]))
                                                {
                                                    if (rob[salah].Contains(io[ui].Trim()))
                                                    {
                                                        
                                                        rob[salah] = rob[salah].Replace(io[ui].Trim(), van[ui].ToString());
                                                       
                                                    }
                                                }
                                                else {
                                                    caseRun(rob[salah]);
                                                }
                                            }
                                           
                                            caseRun(rob[salah]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Parameter Doesn't Match");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Not Found");
                        }
                    }
                    //executes if "moveto" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "moveto") == 0)
                    {
                        String[] parameter1 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("MoveTo Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String[] parameter2 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            String two = parameter2[0];
                            pen_to_move(int.Parse(one), int.Parse(two));
                            if (parameter1.Length > 1 && parameter1.Length < 3)
                                pen_to_move(int.Parse(one), int.Parse(two));
                            else
                                throw new ArgumentException("MoveTo takes Only 2 Parameters");

                        }
                    }
                    else if (m_syntax[0].Equals("\n"))
                    {

                    }
                    //executes if "drawto" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "drawto") == 0)
                    {

                        String[] parameter1 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("DrawTo Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String[] parameter2 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            String two = parameter2[0];
                            pentodraw(int.Parse(one), int.Parse(two));
                            if (parameter1.Length == 2)
                                pentodraw(int.Parse(one), int.Parse(two));

                            else
                            {
                                throw new ArgumentException("DrawTo Takes Only 2 Parameters");
                            }
                        }

                    }
                    //executes if "clear()" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "clear") == 0)
                    {
                        clear();
                    }
                    //executes if "reset()" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "reset") == 0)
                    {
                        reset();
                    }
                    //executes if "rectangle" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "rectangle") == 0)
                    {
                        String[] parameter1 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("Rectangle Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String[] parameter2 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            String two = parameter2[0];
                            if (parameter1.Length > 1 && parameter1.Length < 3)
                                rectangle_draw(position_Xaxis, position_Yaxis, int.Parse(one), int.Parse(two));
                            else
                                throw new ArgumentException("Rectangle Takes Only 2 Parameters");
                        }
                    }

                    else if (string.Compare(m_syntax[0].ToLower(), "square") == 0)
                    {
                        String test = m_syntax[1];
                        String[] parameter2 = m_syntax[1].Split(')');
                        if (!test.Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String two = parameter2[0];
                            if (two != null || two != "" || two != " ")
                                square_draw(position_Xaxis, position_Yaxis, int.Parse(two));
                            else
                                throw new ArgumentException("Square Takes Only 1 Parameter");

                        }
                    }
                    //executes if "circle" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "circle") == 0)
                    {
                        String test = m_syntax[1];
                        String[] parameter2 = m_syntax[1].Split(')');
                        if (!test.Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String two = parameter2[0];
                            if (two != null || two != "" || two != " ")
                                circle_draw(position_Xaxis, position_Yaxis, int.Parse(two));
                            else
                                throw new ArgumentException("Circle Takes Only 1 Parameter");

                        }
                    }
                    //executes if "triangle" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "triangle") == 0)
                    {
                        String[] parameter1 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("Triangle Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String[] parameter2 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            String two = parameter2[0];
                            if (parameter1.Length > 1 && parameter1.Length < 3)
                                triangle_draw(position_Xaxis, position_Yaxis, int.Parse(one), int.Parse(two));
                            else
                                throw new ArgumentException("Triangle Takes Only 2 Parameters");
                        }
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (ArgumentException ecp)
            {
                commandbox.Text = "Error in Line:" + (lineCount ) + " " + ecp.Message;
                panel1.Refresh();
                return false;
            }
            
            catch (Exception ee)
            {
                commandbox.Text = "Error in Line:" + (lineCount) + " " + "parameter Error!!" + ee.Message;
                panel1.Refresh();
                return false;
            }
            
           
        }
        
        /// <summary>
        /// Method for clearing drawing area
        /// </summary>
        public void clear()
        {
            clearing = false;
            panel1.Refresh();
            clearing = true;
        }
        
        /// <summary>
        /// Method for resetting initial position
        /// </summary>
        public void reset()
        {
            reset_bool = false;
            position_Xaxis = 0;
            position_Yaxis = 0;
            reset_bool = true;
        }
        
        /// <summary>
        /// Method for drawing rectangle
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void rectangle_draw(int a, int b,int c, int d )
        {
            drawing = false;
            Rectangle mane = new Rectangle();
            mane.saved_values(a, b, c, d);
            Graphics g = panel1.CreateGraphics();
            mane.Draw_shape(g);
            drawing = true;
        }

        /// <summary>
        /// Method for drawing square
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public void square_draw(int a, int b, int c)
        {
            drawing = false;
            Square mane2 = new Square();
            mane2.saved_values(a, b, c);
            drawing = true;

            Square snj2 = new Square();
            snj2.saved_values(a, b, c);
            Graphics g = panel1.CreateGraphics();
            snj2.Draw_shape(g);
        }
        
        /// <summary>
        /// Method for drawing circle
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public void circle_draw(int a, int b, int c)
        {
            drawing = false;
            Circle mane2 = new Circle();
            mane2.saved_values(a, b, c);
            drawing = true;

            Circle snj2 = new Circle();
            snj2.saved_values(a, b, c);
            Graphics g = panel1.CreateGraphics();
            snj2.Draw_shape(g);
        }
        
        /// <summary>
        /// Method for drawing triangle
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void triangle_draw(int a, int b, int c, int d)
        {
            drawing = false;
            Triangle mane4 = new Triangle();
            mane4.saved_values(a, b, c, d);
            Graphics g = panel1.CreateGraphics();
            mane4.Draw_shape(g);
            drawing = true;
        }

        
        /// <summary>
        /// Button for reset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button1_Click(object sender, EventArgs e)
        {
            reset();
        }
        /// <summary>
        /// triggered when clear button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            clear();
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        
        /// <summary>
        /// Menu for loading files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loading = false;
            OpenFileDialog loadFileDialog = new OpenFileDialog();
            loadFileDialog.Filter = "Text File (.txt)|*.txt";       
            loadFileDialog.Title = "Open File...";

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamReader streamReader = new System.IO.StreamReader(loadFileDialog.FileName);
                programbox.Text = streamReader.ReadToEnd();
                streamReader.Close();
            }
            loading = true;
        }
        

        /// <summary>
        /// Method for saving files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save = true;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text File (.txt)| *.txt";
            saveFileDialog.Title = "Save File...";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter fWriter = new StreamWriter(saveFileDialog.FileName);
                fWriter.Write(programbox.Text);
                fWriter.Close();
                

            }
            save = true;

        }
  
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Works when textbox entered
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
         

            if (commandbox.Text.Equals("clear") == true && e.KeyCode == Keys.Enter)
            {
                clear();
                MessageBox.Show("Drawing area cleared.");        //for clearing drawing area

            }

            if (commandbox.Text.Equals("reset") == true && e.KeyCode == Keys.Enter)
            {
                reset();
                MessageBox.Show("Reset to initial position made.");
            }

            if (commandbox.Text.Equals("run") && e.KeyCode == Keys.Enter)

            {
                variableDictionary.Clear();
                method = false;
                lineCount = 1;
                commandbox.Clear();
                execute = false;
                var multi_command = programbox.Text;
                string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                string command = programbox.Text;
                String[] multiLine = command.Split(')');

                for (int i = 0; i < multiLine.Length - 1; i++)
                {
                    string abc = multiLine[i].Trim();
                    syntax = abc.Split('(');

                    if (syntax[0].Equals("pen") == true)
                    {
                        this.penswitcher(syntax[1]);

                    }

                    else if (syntax[0].Equals("fill") == true)
                    {
                        this.penswitcher1(syntax[1]);
                        fill = true;
                        if (syntax[1] == "off")
                        {
                            fill = false;
                        }

                    }

                }




                foreach (string l in multi_syntax)
                {
                    bool result = caseRun(l);
                    if (!result)
                    {
                        panel1.Invalidate();
                        break;
                    }
                    lineCount++;
                }
            }



        }

        /// <summary>
        /// Menu for exiting application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }


        /// <summary>
        /// Menu for instructions about application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Instructions: \n" +
                "For Example: \n Programs: circle(70) || triangle(80, 50, 100) || " +
                "rectangle(50,50)  || moveto(50,50 ||drawto(50,50) || fill(red)" +
                "|| fill(off) || pen(blue) || pen(green) || pen(yellow) || pen(red)" +
                "|| r=30 \n count=1\n loop forcount <=5\n circle(l,b)\n r+20\n count+1\nendloop\n " +
                "Similarly, methods also can be made." +
                "Commands: run || clear || reset");
        }

        /// <summary>
        /// Menu for aboutbox that shows about the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("About: \n This is a program designed to produce different shapes" +
                "\n"
                + "from the user input taken from textbox. For e.g: circle,"
                + "\n" + "triangle, rectangle and square. Moveto  moves the position and" +
                "reset changes position to initial. Command run executes all the program." +
                "Clear clears the drawing area. Drawto draws a line from the size given. Fill " +
                "on fills the shapes with choosed color and fill of doesnot make shape fill." +
                "There are 4 different pen colors and users can choose it from textbox. User can " +
                "also use loop for creating many shapes at once. Methods can be made to assign the " +
                "shape names. Parameter names can be user defined. This application has these user " +
                "defined features. " + "\n" +
                "Designed by: Joshna Poudel");
        }
    }


}
    