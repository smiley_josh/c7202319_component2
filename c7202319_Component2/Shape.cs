﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace c7202319_Component2
{
    /// <summary>
    /// Interface for Shape that implements shape classes
    /// </summary>
   interface Shape
    {
        
        void Draw_shape(Graphics g);   //method for drawing shape
    }
}
